package nl.app.com.revoluttest.presentation.currency.ui

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager


class NPALinearLayoutManager(ctx: Context) : LinearLayoutManager(ctx) {

    override fun supportsPredictiveItemAnimations(): Boolean {
        return false
    }
}