package nl.app.com.revoluttest.presentation.currency.model

const val ID_CLICK = -165
const val ID_EDIT = -166
const val ID_EDIT_FINISHED = -167
sealed class UIEvent {
     open class Base(val id: Int) : UIEvent()
     data class CurrencyClick(val item: Currency) : Base(ID_CLICK)
     data class Edit(val text: String) : Base(ID_EDIT)
     class EditFinished(): Base(ID_EDIT_FINISHED)
}