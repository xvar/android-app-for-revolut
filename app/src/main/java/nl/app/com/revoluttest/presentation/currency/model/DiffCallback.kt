package nl.app.com.revoluttest.presentation.currency.model

import androidx.recyclerview.widget.DiffUtil

class DiffCallback: DiffUtil.ItemCallback<DiffItem>() {
    override fun areItemsTheSame(oldItem: DiffItem, newItem: DiffItem): Boolean {
        return oldItem.areItemsTheSame(newItem)
    }

    override fun areContentsTheSame(oldItem: DiffItem, newItem: DiffItem): Boolean {
        return oldItem.areContentsTheSame(newItem)
    }

    override fun getChangePayload(oldItem: DiffItem, newItem: DiffItem): Any? {
        return newItem.createPayloads(oldItem)
    }
}