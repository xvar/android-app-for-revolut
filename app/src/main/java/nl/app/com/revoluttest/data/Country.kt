package nl.app.com.revoluttest.data

import androidx.annotation.DrawableRes

data class Country(val code: String, @DrawableRes val flagId : Int, val curName : String)