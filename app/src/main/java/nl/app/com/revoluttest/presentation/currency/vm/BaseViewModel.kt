package nl.app.com.revoluttest.presentation.currency.vm

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import nl.app.com.revoluttest.core.DisposableMap

open class BaseViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val compositeDisposableMap = DisposableMap()

    fun addDisposable(d: Disposable) = compositeDisposable.add(d)
    fun addDisposable(key: String, d: Disposable) = compositeDisposableMap.add(key, d)

    fun clearAll() {
        compositeDisposable.clear()
        compositeDisposableMap.clear()
    }

    override fun onCleared() {
        super.onCleared()
        clearAll()
    }
}