package nl.app.com.revoluttest.presentation.image

import android.widget.ImageView
import com.squareup.picasso.Transformation

interface ImageLoader {
    fun loadImage(url: String, sidePx: Int, target: ImageView, transformation: Transformation? = null)
    fun loadImage(id: Int, sidePx: Int, target: ImageView, transformation: Transformation? = null)
}