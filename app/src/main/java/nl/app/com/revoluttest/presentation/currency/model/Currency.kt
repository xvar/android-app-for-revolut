package nl.app.com.revoluttest.presentation.currency.model

import androidx.annotation.DrawableRes

data class Currency(
        val code: String,
        val amount: String,
        @DrawableRes val imageId : Int,
        val description: String = "",
        val editTextEditable: Boolean = false
) : DiffItem {

    override fun getStableId(): Long = code.hashCode().toLong()

    override fun areItemsTheSame(item: DiffItem) : Boolean {
        return item is Currency && this.code == item.code
    }

    override fun areContentsTheSame(item: DiffItem) : Boolean {
        return item is Currency && this == item
    }

    override fun createPayloads(oldItem: DiffItem): Payload = Payload.Currency(amount, editTextEditable)
}
