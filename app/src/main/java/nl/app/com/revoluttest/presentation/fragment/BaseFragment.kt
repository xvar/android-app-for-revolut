package nl.app.com.revoluttest.presentation.fragment

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import nl.app.com.revoluttest.core.DisposableMap
import nl.app.com.revoluttest.core.di.Injectable
import nl.app.com.revoluttest.core.di.ViewModelFactory
import javax.inject.Inject

abstract class BaseFragment : Fragment(), Injectable {

    private val compositeDisposable = CompositeDisposable()
    private val compositeDisposableMap = DisposableMap()

    protected fun addDisposable(d: Disposable) = compositeDisposable.add(d)
    protected fun addDisposable(key: String, d: Disposable) = compositeDisposableMap.add(key, d)
    protected fun clearDisposables() {
        compositeDisposable.clear()
        compositeDisposableMap.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        clearDisposables()
    }

    inline fun <reified T : ViewModel> ViewModelFactory<T>.get(): T =
            ViewModelProviders.of(this@BaseFragment, this)[T::class.java]

    inline fun <reified T : ViewModel> ViewModelFactory<T>.getForActivity(): T =
            ViewModelProviders.of(activity!!, this)[T::class.java]
}