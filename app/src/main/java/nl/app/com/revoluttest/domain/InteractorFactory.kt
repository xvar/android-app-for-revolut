package nl.app.com.revoluttest.domain

import nl.app.com.revoluttest.core.Executors
import nl.app.com.revoluttest.data.CountryRepository
import nl.app.com.revoluttest.data.CurrencyRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InteractorFactory @Inject constructor(
        private val repository: CurrencyRepository,
        private val currencyHolder: CurrencyHolder,
        private val countryRepository: CountryRepository,
        private val executors: Executors
) {
    private val currencyInteractor : CurrencyInteractor by lazy {
        CurrencyInteractor(repository, currencyHolder, countryRepository, executors)
    }

    fun provideInteractor() = currencyInteractor
}