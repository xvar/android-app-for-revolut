package nl.app.com.revoluttest.data

import nl.app.com.revoluttest.R
import javax.inject.Inject

private const val EUR = "EUR"
private const val CHF = "CHF"
private const val INR = "INR"
private const val USD = "USD"
private const val ISK = "ISK"
private const val IDR = "IDR"
private const val MXN = "MXN"
private const val ZAR = "ZAR"
private const val HRK = "HRK"
private const val CNY = "CNY"
private const val THB = "THB"
private const val AUD = "AUD"
private const val ILS = "ILS"
private const val KRW = "KRW"
private const val JPY = "JPY"
private const val PLN = "PLN"
private const val GBP = "GBP"
private const val HUF = "HUF"
private const val PHP = "PHP"
private const val TRY = "TRY"
private const val RUB = "RUB"
private const val HKD = "HKD"
private const val DKK = "DKK"
private const val CAD = "CAD"
private const val MYR = "MYR"
private const val BGN = "BGN"
private const val NOK = "NOK"
private const val RON = "RON"
private const val SGD = "SGD"
private const val CZK = "CZK"
private const val SEK = "SEK"
private const val NZD = "NZD"
private const val BRL = "BRL"

class CountryRepository @Inject constructor(private val r: ResourceRepository) {

    fun get(code: String) : Country {
        return when (code) {
            EUR -> Country(EUR, R.drawable.flag_eur, r.getString(R.string.cur_name_eur))
            CHF -> Country(CHF, R.drawable.flag_chf, r.getString(R.string.cur_name_chf))
            INR -> Country(INR, R.drawable.flag_inr, r.getString(R.string.cur_name_inr))
            USD -> Country(USD, R.drawable.flag_usd, r.getString(R.string.cur_name_usd))
            ISK -> Country(ISK, R.drawable.flag_isk, r.getString(R.string.cur_name_isk))
            IDR -> Country(IDR, R.drawable.flag_idr, r.getString(R.string.cur_name_idr))
            MXN -> Country(MXN, R.drawable.flag_mxn, r.getString(R.string.cur_name_mxn))
            ZAR -> Country(ZAR, R.drawable.flag_zar, r.getString(R.string.cur_name_zar))
            HRK -> Country(HRK, R.drawable.flag_hrk, r.getString(R.string.cur_name_hrk))
            CNY -> Country(CNY, R.drawable.flag_cny, r.getString(R.string.cur_name_cny))
            THB -> Country(THB, R.drawable.flag_thb, r.getString(R.string.cur_name_thb))
            AUD -> Country(AUD, R.drawable.flag_aud, r.getString(R.string.cur_name_aud))
            ILS -> Country(ILS, R.drawable.flag_ils, r.getString(R.string.cur_name_ils))
            KRW -> Country(KRW, R.drawable.flag_krw, r.getString(R.string.cur_name_krw))
            JPY -> Country(JPY, R.drawable.flag_jpy, r.getString(R.string.cur_name_jpy))
            PLN -> Country(PLN, R.drawable.flag_pln, r.getString(R.string.cur_name_pln))
            GBP -> Country(GBP, R.drawable.flag_gbp, r.getString(R.string.cur_name_gbp))
            HUF -> Country(HUF, R.drawable.flag_huf, r.getString(R.string.cur_name_huf))
            PHP -> Country(PHP, R.drawable.flag_php, r.getString(R.string.cur_name_php))
            TRY -> Country(TRY, R.drawable.flag_try, r.getString(R.string.cur_name_try))
            RUB -> Country(RUB, R.drawable.flag_rub, r.getString(R.string.cur_name_rub))
            HKD -> Country(HKD, R.drawable.flag_hkd, r.getString(R.string.cur_name_hkd))
            DKK -> Country(DKK, R.drawable.flag_dkk, r.getString(R.string.cur_name_dkk))
            CAD -> Country(CAD, R.drawable.flag_cad, r.getString(R.string.cur_name_cad))
            MYR -> Country(MYR, R.drawable.flag_myr, r.getString(R.string.cur_name_myr))
            BGN -> Country(BGN, R.drawable.flag_bgn, r.getString(R.string.cur_name_bgn))
            NOK -> Country(NOK, R.drawable.flag_nok, r.getString(R.string.cur_name_nok))
            RON -> Country(RON, R.drawable.flag_ron, r.getString(R.string.cur_name_ron))
            SGD -> Country(SGD, R.drawable.flag_sgd, r.getString(R.string.cur_name_sgd))
            CZK -> Country(CZK, R.drawable.flag_czk, r.getString(R.string.cur_name_czk))
            SEK -> Country(SEK, R.drawable.flag_sek, r.getString(R.string.cur_name_sek))
            NZD -> Country(NZD, R.drawable.flag_nzd, r.getString(R.string.cur_name_nzd))
            BRL -> Country(BRL, R.drawable.flag_brl, r.getString(R.string.cur_name_brl))
            else -> Country("unknown", R.drawable.flag_eur, r.getString(R.string.cur_name_unknown))
        }
    }

}