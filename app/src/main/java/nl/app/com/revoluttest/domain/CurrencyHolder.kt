package nl.app.com.revoluttest.domain

import androidx.annotation.AnyThread
import io.reactivex.functions.Function
import nl.app.com.revoluttest.data.CodeMapDTO
import nl.app.com.revoluttest.data.CurrenciesDTO
import java.util.concurrent.locks.ReentrantLock
import java.util.concurrent.locks.ReentrantReadWriteLock
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.concurrent.read
import kotlin.concurrent.write

/**
 * This class encapsulates logic of adding new currency list, changing base currency code
 * Thread-safe
 */

typealias CurrencyDTOMapper = Function<CurrenciesDTO, List<CurrencyDomain>>
private const val DEFAULT_CURRENCY = "EUR"

@AnyThread
@Singleton
class CurrencyHolder @Inject constructor() : CurrencyDTOMapper {

    /**
     * selected (base) currency is list's first entry
     */
    private var baseAmount : Float = 100f
    private var baseCode : String? = null
    private val curList = ArrayList<CodeMapDTO>()
    private val curToIndexMap = HashMap<String, Int>()
    private val lock = ReentrantReadWriteLock()

    fun baseCodeChanged(code: String) : Boolean = lock.read {
        if (baseCode == null)
            return false
        return !baseCode.equals(code)
    }

    override fun apply(response: CurrenciesDTO): List<CurrencyDomain> = lock.write {
        if (curList.isEmpty()) {
            initCurrencyList(response)
        } else {
            //order must be preserved
            changeBase(response.base)
            updateMultipliers(response.rates)
        }
        return getCurrent()
    }

    private fun updateMultipliers(list: List<CodeMapDTO>) = lock.write {
        list.forEach {
            if (it.multiplier.isValid()) {
                val index = curToIndexMap[it.currCode]
                if (index != null) {
                    curList[index].multiplier = it.multiplier
                }
            }
        }
    }

    fun changeBase(newBase: String) = lock.write {
        val index = curList.indexOfFirst { it.currCode == newBase }
        if (index != -1) {
            val elem = curList[index]

            baseAmount *= elem.multiplier
            val curChangeMultiplier = 1 / elem.multiplier

            curList.removeAt(index)
            curList.add(0, elem)

            curList.forEach { it.multiplier *= curChangeMultiplier }
            updateIndex()
            baseCode = newBase
        }
    }

    private fun initCurrencyList(response: CurrenciesDTO) = lock.write {
        response.rates.forEach { if (it.multiplier.isValid()) curList.add(it) }
        curList.add(0, CodeMapDTO(response.base, 1.0f))
        updateIndex()
    }

    private fun updateIndex() = lock.write {
        curList.forEachIndexed { index, elem -> curToIndexMap[elem.currCode] = index }
    }

    private fun Float.isValid() = !this.isNaN() && this != 0.0f

    fun getCurrent() : List<CurrencyDomain> = lock.read {
        return@read curList.mapIndexed { index, element ->
            CurrencyDomain(element.currCode, element.multiplier * baseAmount, index)
        }
    }

    fun getBase(): String? = lock.read {  baseCode ?: DEFAULT_CURRENCY }

    fun changeBaseAmount(amountUpdate: Float) = lock.write {
        baseAmount = amountUpdate
    }

}