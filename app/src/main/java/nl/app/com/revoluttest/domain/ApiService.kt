package nl.app.com.revoluttest.domain

import io.reactivex.Flowable
import nl.app.com.revoluttest.data.CurrenciesDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

        @GET("/latest?")
        fun getCurrencies(@Query("base") currency: String) : Flowable<CurrenciesDTO>

}