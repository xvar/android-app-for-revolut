package nl.app.com.revoluttest.data

data class CodeMapDTO(val currCode : String, var multiplier: Float)
