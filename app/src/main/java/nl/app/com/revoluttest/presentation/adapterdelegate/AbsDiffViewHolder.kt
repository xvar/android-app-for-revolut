package nl.app.com.revoluttest.presentation.adapterdelegate

import android.view.View
import io.reactivex.disposables.Disposable
import nl.app.com.revoluttest.core.DisposableMap
import nl.app.com.revoluttest.presentation.currency.model.Payload
import nl.app.com.revoluttest.presentation.currency.model.UIEvent
import org.reactivestreams.Subscriber

abstract class AbsDiffViewHolder<T> (
        itemView: View,
        protected val subscriber: Subscriber<UIEvent>
) : DelegateViewHolder<T, Payload>(itemView) {

    private val compositeDisposableMap = DisposableMap()
    protected fun addDisposable(key: String, d: Disposable) = compositeDisposableMap.add(key, d)
    protected fun clearDisposable(key: String) = compositeDisposableMap.clear(key)
    protected fun clearDisposables() = compositeDisposableMap.clear()

    override fun bind(payloads: List<Payload>) {
        super.bind(payloads)

        for (item in payloads) {
            handle(item)
        }
    }

    private val payloadHandlers = mutableMapOf<Int, (Payload) -> Unit>()

    protected open fun handle(item: Payload) {
        payloadHandlers[item.id]?.invoke(item)
    }

    protected fun registerHandler(id : Int, handler: (Payload) -> Unit) {
        payloadHandlers[id] = handler
    }
}