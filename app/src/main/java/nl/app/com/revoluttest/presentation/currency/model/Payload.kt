package nl.app.com.revoluttest.presentation.currency.model

const val ID_CUR = -2001

sealed class Payload(val id: Int) {
    class Currency(val newAmount: String, val isEditable: Boolean) : Payload(ID_CUR)
}