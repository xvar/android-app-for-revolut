package nl.app.com.revoluttest.data

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.forEach


class CodeMapDeserializer : JsonDeserializer<List<CodeMapDTO>>() {

    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): List<CodeMapDTO> {
        val mapper = ObjectMapper()
        val typeFactory = mapper.typeFactory
        val mapType = typeFactory.constructMapType(HashMap::class.java, String::class.java, Float::class.java)
        val map : Map<String, Float> = mapper.readValue(p, mapType)
        val list = ArrayList<CodeMapDTO>()
        map.forEach{ (key, value) -> list.add(CodeMapDTO(key, value))}
        return list
    }

}