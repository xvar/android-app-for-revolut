package nl.app.com.revoluttest.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nl.app.com.revoluttest.presentation.currency.ui.CurrencyActivity

@Suppress("unused")
@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector()
    abstract fun contributeCurrencyActivity(): CurrencyActivity
}