package nl.app.com.revoluttest.presentation.image

import android.graphics.*
import android.graphics.Bitmap
import com.squareup.picasso.Transformation
import android.graphics.RectF
import android.graphics.Shader
import android.graphics.BitmapShader


class CircleTransformation : Transformation{
    override fun transform(source: Bitmap): Bitmap {
        val minEdge = Math.min(source.width, source.height)
        val dx = ((source.width - minEdge) / 2).toFloat()
        val dy = ((source.height - minEdge) / 2).toFloat()

        // Init shader
        val shader = BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        val matrix = Matrix()
        matrix.setTranslate(-dx, -dy)   // Move the target area to center of the source bitmap
        shader.setLocalMatrix(matrix)

        // Init paint
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.shader = shader

        // Create and draw circle bitmap
        val config = if (source.config != null) source.config else Bitmap.Config.ARGB_8888
        val output = Bitmap.createBitmap(minEdge, minEdge, config)
        val canvas = Canvas(output)
        canvas.drawOval(RectF(0f, 0f, minEdge.toFloat(), minEdge.toFloat()), paint)

        source.recycle()
        return output
    }

    override fun key(): String {
        return "circle"
    }
}