package nl.app.com.revoluttest.data

import java.util.*

fun IntRange.random() =
        Random().nextInt((endInclusive + 1) - start) +  start