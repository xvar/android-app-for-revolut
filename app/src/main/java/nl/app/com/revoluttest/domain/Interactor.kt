package nl.app.com.revoluttest.domain

import io.reactivex.Flowable

interface Interactor<out T, P> {
    fun create(params : P? = null) : Flowable<out T>
}