package nl.app.com.revoluttest.presentation.currency.ui

import android.view.LayoutInflater
import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import nl.app.com.revoluttest.presentation.currency.model.DiffItem
import nl.app.com.revoluttest.presentation.currency.model.UIEvent
import nl.app.com.revoluttest.presentation.image.ImageLoader

class CurrencyAdapter(
        callback: DiffUtil.ItemCallback<DiffItem>,
        layoutInflater: LayoutInflater,
        imageLoader: ImageLoader
) : AsyncListDifferDelegationAdapter<DiffItem>(callback) {

    private val processor = PublishProcessor.create<UIEvent>()
    fun events() : Flowable<UIEvent> = processor

    init {
        delegatesManager.addDelegate(CurrencyDelegate(processor, imageLoader, layoutInflater))
    }

    override fun getItemId(position: Int): Long {
        return try {
            items[position].getStableId()
        } catch (ignored: IndexOutOfBoundsException) {
            super.getItemId(position)
        }
    }

}