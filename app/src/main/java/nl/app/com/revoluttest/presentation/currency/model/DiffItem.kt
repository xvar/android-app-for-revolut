package nl.app.com.revoluttest.presentation.currency.model

interface DiffItem {
    fun areItemsTheSame(item : DiffItem) : Boolean
    fun areContentsTheSame(item: DiffItem) : Boolean
    fun createPayloads(oldItem: DiffItem) : Payload
    fun getStableId() : Long
}