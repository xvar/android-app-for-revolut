package nl.app.com.revoluttest.data

import io.reactivex.Flowable
import nl.app.com.revoluttest.domain.ApiService
import javax.inject.Inject

class CurrencyRepository @Inject constructor(private val apiService: ApiService) {

    fun getCurrencies(base: String?): Flowable<CurrenciesDTO> {
        return if (base == null) {
            Flowable.empty()
        } else {
            apiService.getCurrencies(base)
        }
    }
}