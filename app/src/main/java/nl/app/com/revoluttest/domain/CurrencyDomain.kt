package nl.app.com.revoluttest.domain

data class CurrencyDomain(val code: String, val amount: Float, val position: Int)