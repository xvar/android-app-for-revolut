package nl.app.com.revoluttest.presentation.currency.model

import androidx.annotation.StringRes
import nl.app.com.revoluttest.R
import nl.app.com.revoluttest.data.ConnectivityException

sealed class UIState {
    data class Loading(val isLoading: Boolean) : UIState()
    data class Error(@StringRes val error: Int) : UIState()
    data class CurrencyData(val currencies: List<Currency>) : UIState()
}

fun Throwable.textId() = if (this is ConnectivityException) R.string.error_connectivity else R.string.error_backend