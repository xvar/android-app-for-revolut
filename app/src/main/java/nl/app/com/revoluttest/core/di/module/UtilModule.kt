package nl.app.com.revoluttest.core.di.module

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import nl.app.com.revoluttest.data.ResourceRepository
import nl.app.com.revoluttest.presentation.image.ImageLoader
import nl.app.com.revoluttest.presentation.image.ImageLoaderPicassoImpl
import javax.inject.Singleton

@Module
class UtilModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(app: Application) : SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
    }

    @Provides
    @Singleton
    fun providePicasso(app: Application): Picasso {
        val picasso = Picasso.Builder(app.applicationContext)
                //.downloader(OkHttp3Downloader(client))
                .build()
        Picasso.setSingletonInstance(picasso)
        return picasso
    }

    @Provides
    @Singleton
    fun providePicassoImageLoader(picasso: Picasso): ImageLoader {
        return ImageLoaderPicassoImpl(picasso)
    }

    @Provides
    @Singleton
    fun provideResourceProvider(app: Application) = ResourceRepository(app)


}