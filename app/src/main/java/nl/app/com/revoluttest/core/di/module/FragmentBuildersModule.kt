package nl.app.com.revoluttest.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nl.app.com.revoluttest.presentation.currency.ui.CurrencyFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeCurrencyFragment(): CurrencyFragment
}