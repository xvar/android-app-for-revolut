package nl.app.com.revoluttest.data

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

data class CurrenciesDTO(
        @JsonProperty("base") val base: String,
        @JsonProperty("date") val date: String,

        @JsonProperty("rates")
        @JsonDeserialize(using = CodeMapDeserializer::class)
        val rates: List<CodeMapDTO>
)
