package nl.app.com.revoluttest.presentation.currency.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import nl.app.com.revoluttest.R
import nl.app.com.revoluttest.presentation.adapterdelegate.AbsListDelegate
import nl.app.com.revoluttest.presentation.currency.model.Currency
import nl.app.com.revoluttest.presentation.currency.model.DiffItem
import nl.app.com.revoluttest.presentation.currency.model.Payload
import nl.app.com.revoluttest.presentation.currency.model.UIEvent
import nl.app.com.revoluttest.presentation.image.ImageLoader
import org.reactivestreams.Subscriber

class CurrencyDelegate(
        private val sub: Subscriber<UIEvent>,
        private val iml: ImageLoader,
        private val layoutInflater: LayoutInflater
) : AbsListDelegate<Currency, DiffItem, CurrencyViewHolder, Payload>() {

    override fun onCreateViewHolder(parent: ViewGroup): CurrencyViewHolder {
        return CurrencyViewHolder(
                containerView = layoutInflater.inflate(R.layout.currency_item, parent, false),
                sub = sub,
                imageLoader = iml
        )
    }

    override fun isForViewType(item: DiffItem, items: MutableList<DiffItem>, position: Int): Boolean = item is Currency

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        if (holder is CurrencyViewHolder) { holder.clear() }
    }
}