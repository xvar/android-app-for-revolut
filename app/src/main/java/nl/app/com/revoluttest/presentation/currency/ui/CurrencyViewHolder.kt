package nl.app.com.revoluttest.presentation.currency.ui

import android.text.InputType
import android.view.View
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.currency_item.*
import nl.app.com.revoluttest.R
import nl.app.com.revoluttest.core.hideKeyboard
import nl.app.com.revoluttest.presentation.adapterdelegate.AbsDiffViewHolder
import nl.app.com.revoluttest.presentation.currency.model.Currency
import nl.app.com.revoluttest.presentation.currency.model.ID_CUR
import nl.app.com.revoluttest.presentation.currency.model.Payload
import nl.app.com.revoluttest.presentation.currency.model.UIEvent
import nl.app.com.revoluttest.presentation.image.ImageLoader
import org.reactivestreams.Subscriber
import java.util.concurrent.TimeUnit

private const val KEY_TEXT_CHANGES = "text_changes"
private const val TEXT_CHANGES_TIMEOUT_MS = 50L
class CurrencyViewHolder(
        override val containerView: View,
        sub: Subscriber<UIEvent>,
        private val imageLoader: ImageLoader
) : AbsDiffViewHolder<Currency>(containerView, sub), LayoutContainer {

    init {
        registerHandler(ID_CUR) { p -> applyCurrency((p as Payload.Currency)) }
    }
    private val clickListener = CurrencyListener(sub)

    override fun bind(item : Currency) {
        removeTextChangeListeners()
        if (item.imageId != 0) {
            imageLoader.loadImage(
                    item.imageId,
                    image.context.resources.getDimensionPixelSize(R.dimen.currency_image_side),
                    image
            )
        }
        setEditable(item.editTextEditable)
        code.text = item.code
        description.text = item.description
        setAmount(item.amount)
        clickListener.updateItem(item)
        container.setOnClickListener(clickListener)
        addTextChangeListeners(item.editTextEditable)
    }

    private fun applyCurrency(currency: Payload.Currency) {
        removeTextChangeListeners()
        setAmount(currency.newAmount)
        setEditable(currency.isEditable)
        addTextChangeListeners(currency.isEditable)
    }

    private fun removeTextChangeListeners() = clearDisposables()
    private fun addTextChangeListeners(isItemEditable: Boolean) {
        if (isItemEditable) {
            val textChanges = amount.textChangeEvents()
                    .skipInitialValue()
            addDisposable(KEY_TEXT_CHANGES, textChanges
                    .retry()
                    .observeOn(AndroidSchedulers.mainThread())
                    .doAfterNext { amount.postDelayed( { subscriber.onNext(UIEvent.EditFinished()) }, TEXT_CHANGES_TIMEOUT_MS) }
                    .subscribe({ event ->
                                subscriber.onNext(UIEvent.Edit(event.text.toString()))
                                lastSelection = amount.selectionEnd
                            },
                            {}
                    )
            )
        }
    }

    private fun setEditable(isEditable: Boolean) {
        if (isEditable) {
            amount.isFocusable = true
            amount.isFocusableInTouchMode = true
            amount.isClickable = true
            amount.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            amount.setOnClickListener(null)
        } else {
            amount.isFocusable = false
            amount.isFocusableInTouchMode = false
            amount.isClickable = false
            amount.inputType = InputType.TYPE_NULL
            amount.setOnClickListener(clickListener)
        }
    }

    private var lastSelection = 0
    private fun setAmount(newAmount: String) {
        amount.setText(newAmount)
        val textStr = amount.text.toString()
        val textStrLen = textStr.length
        when {
            textStr.isNotEmpty() && lastSelection > 0 && lastSelection <= textStrLen -> amount.setSelection(lastSelection)
            else -> amount.setSelection(amount.text.length)
        }
    }

    fun clear() {
        clearDisposables()
    }
}

private class CurrencyListener(private val s: Subscriber<UIEvent>) : View.OnClickListener {
    private lateinit var item : Currency

    fun updateItem(item: Currency) {
        this.item = item
    }

    override fun onClick(v: View?) {
        v?.hideKeyboard()
        s.onNext(UIEvent.CurrencyClick(item))
    }
}
