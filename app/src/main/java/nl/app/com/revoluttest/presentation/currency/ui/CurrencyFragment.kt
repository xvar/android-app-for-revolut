package nl.app.com.revoluttest.presentation.currency.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.currency_fragment.*
import nl.app.com.revoluttest.R
import nl.app.com.revoluttest.core.di.ViewModelFactory
import nl.app.com.revoluttest.presentation.currency.model.Currency
import nl.app.com.revoluttest.presentation.currency.model.DiffCallback
import nl.app.com.revoluttest.presentation.currency.model.DiffItem
import nl.app.com.revoluttest.presentation.currency.model.UIState
import nl.app.com.revoluttest.presentation.currency.vm.CurrencyViewModel
import nl.app.com.revoluttest.presentation.fragment.BaseFragment
import nl.app.com.revoluttest.presentation.image.ImageLoader
import javax.inject.Inject


private const val KEY_DATA = "key_data"
private const val KEY_CLICKS = "key_clicks"
private const val KEY_LOADING = "key_loading"
private const val KEY_ERROR = "key_error"
class CurrencyFragment : BaseFragment() {

    companion object {
        fun newInstance() = CurrencyFragment()
    }

    private lateinit var adapter: CurrencyAdapter

    @Inject lateinit var imageLoader: ImageLoader
    @Inject lateinit var vmFactory: ViewModelFactory<CurrencyViewModel>
    private lateinit var viewModel: CurrencyViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.currency_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currenciesList.layoutManager = NPALinearLayoutManager(activity!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = vmFactory.get()
        adapter = CurrencyAdapter(DiffCallback(), LayoutInflater.from(activity), imageLoader)
        adapter.setHasStableIds(true)
        currenciesList.adapter = adapter
        setupBindings()
        viewModel.init()
    }

    private fun setupBindings() {
        //list
        val uiState = viewModel.getUiState()

        addDisposable(KEY_DATA,
                uiState
                        .filter { it is UIState.CurrencyData }
                        .map { it as UIState.CurrencyData }
                        .subscribe ( { showData(it) }, { Log.e("m_debug", "$it")} )
        )
        //loading
        addDisposable(KEY_LOADING,
                uiState
                        .filter { it is UIState.Loading }
                        .map { it as UIState.Loading }
                        .subscribe ({ showLoading(it.isLoading) }, { Log.e("m_debug", "$it")} )
        )
        //click events
        addDisposable(KEY_CLICKS, adapter.events().subscribe(viewModel))
        //errors
        addDisposable(KEY_ERROR,
                uiState
                        .filter{ it is UIState.Error }
                        .map { it as UIState.Error }
                        .subscribe( { showToast(it.error) }, { Log.e("m_debug", "$it")} )
        )
    }

    private fun showLoading(isLoading: Boolean) {
        progress.isVisible = isLoading
        currenciesList.isVisible = !isLoading
    }

    private fun showData(data: UIState.CurrencyData) {
        if (data.currencies.isNotEmpty()) {
            val scroll = shouldScrollToTop(adapter.items, data.currencies)
            adapter.items = data.currencies
            if (scroll) {
                currenciesList.addOnLayoutChangeListener(OneTimeLayoutChangeListener(currenciesList) {
                    currenciesList.scrollToPosition(0)
                })
            }
        }
    }

    private fun shouldScrollToTop(adapterList: List<DiffItem>?, updateList: List<Currency>): Boolean {
        var scroll = false
        if (adapterList != null && adapterList.isNotEmpty()) {
            scroll = adapterList[0] != updateList[0]
        }
        return scroll
    }

    private fun showToast(@StringRes id: Int) {
        Toast.makeText(activity, id, Toast.LENGTH_LONG).show()
    }

}
