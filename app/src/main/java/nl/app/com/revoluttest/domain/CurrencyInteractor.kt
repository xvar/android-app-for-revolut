package nl.app.com.revoluttest.domain

import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import nl.app.com.revoluttest.core.DisposableMap
import nl.app.com.revoluttest.core.Executors
import nl.app.com.revoluttest.data.CountryRepository
import nl.app.com.revoluttest.data.CurrencyRepository
import nl.app.com.revoluttest.presentation.currency.model.Currency
import nl.app.com.revoluttest.presentation.currency.model.UIState
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

private const val KEY_REPOSITORY = "key_repository"
private const val KEY_BASE = "key_base"
private const val PERIODIC_TIMEOUT = 1L
class CurrencyInteractor constructor(
        private val repository: CurrencyRepository,
        private val currencyHolder: CurrencyHolder,
        private val countryRepository: CountryRepository,
        private val executors: Executors
): Interactor<UIState, Unit> {

    private val dMap = DisposableMap()
    private val state = BehaviorProcessor.create<UIState>()
    private val periodicUpdateIsLocked = AtomicBoolean(false)

    override fun create(params: Unit?): Flowable<out UIState> {
        reSubscribe()
        return state
    }

    private fun reSubscribe() {
        val requestCurrency = repository.getCurrencies(currencyHolder.getBase())
                .map { currencyHolder.apply(it) }
                .concatMapIterable { it -> it }
                .map { it -> Pair(it, countryRepository.get(it.code)) }
                .map { it ->
                    val cur = it.first
                    val country = it.second
                    Currency(
                            cur.code,
                            amountStr(cur.amount),
                            country.flagId,
                            country.curName,
                            cur.position == 0
                    )
                }
                .toList()
                .map { UIState.CurrencyData(it) }
                .toFlowable()

        val repeatedRequest = Flowable.interval(0, PERIODIC_TIMEOUT, TimeUnit.SECONDS)
                .flatMap {
                    requestCurrency
                }
                .retry() //for production there might be exponential backoff
                .filter { it.currencies.isNotEmpty() }


        dMap.add(KEY_REPOSITORY,
                repeatedRequest
                        .subscribeOn(executors.io())
                        .observeOn(executors.main())
                        .filter { data: UIState.CurrencyData -> data.currencies.isNotEmpty() }
                        .subscribe { ui ->
                            if (!periodicUpdateIsLocked.get() &&
                                    !currencyHolder.baseCodeChanged(ui.currencies[0].code)) {
                                state.onNext(ui)
                            }
                        }
        )
    }

    fun changeBase(code : String) {
        currencyHolder.changeBase(code)
        notifyBaseUpdate()
    }

    private val baseUpdates =
            Flowable.fromCallable { currencyHolder.getCurrent() }
            .concatMapIterable { it -> it }
            .map { it -> Pair(it, countryRepository.get(it.code)) }
            .map { it ->
                val cur = it.first
                val country = it.second
                Currency(
                        cur.code,
                        amountStr(cur.amount),
                        country.flagId,
                        country.curName,
                        cur.position == 0
                )
            }
            .toList()
            .map { UIState.CurrencyData(it) }
            .subscribeOn(executors.io())
            .observeOn(executors.main())

    private fun notifyBaseUpdate() {
        dMap.add(KEY_BASE,
                baseUpdates
                        .doOnSubscribe { periodicUpdateIsLocked.set(true) }
                        .doAfterSuccess { periodicUpdateIsLocked.set(false); reSubscribe() }
                        .subscribe { ui ->
                            state.onNext(ui)
                        }
        )
    }

    private fun notifyEditUpdate() {
        dMap.add(KEY_BASE,
                baseUpdates
                        .doOnSubscribe { periodicUpdateIsLocked.set(true) }
                        .subscribe { ui ->
                            state.onNext(ui)
                        }
        )
    }

    private val formatSymbols = DecimalFormatSymbols().apply { decimalSeparator = '.' }
    private val format = DecimalFormat().apply {
        isDecimalSeparatorAlwaysShown = false
        decimalFormatSymbols = formatSymbols
        isGroupingUsed = false
    }

    private fun amountStr(amount: Float) : String {
        return if (amount == 0f) "" else format.format(amount)
    }

    fun changeBaseAmount(text: String) {
        val amountUpdate = if (text.isBlank()) 0f else text.toFloatOrNull() ?: 0f
        currencyHolder.changeBaseAmount(amountUpdate)
        notifyEditUpdate()
    }

    fun clear() = dMap.clear()
    fun update() {
        periodicUpdateIsLocked.set(false)
    }

}

