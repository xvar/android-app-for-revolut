package nl.app.com.revoluttest.presentation.currency.ui

import android.view.View

class OneTimeLayoutChangeListener(val view: View?, val action: () -> Unit) : View.OnLayoutChangeListener {
    override fun onLayoutChange(v: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
        view?.removeOnLayoutChangeListener(this)
        action()
    }
}