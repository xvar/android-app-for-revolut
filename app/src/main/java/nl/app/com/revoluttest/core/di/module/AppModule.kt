package nl.app.com.revoluttest.core.di.module

import dagger.Module

@Module(includes = [
    UtilModule::class, ActivityBuildersModule::class, FragmentBuildersModule::class, NetworkModule::class
])
class AppModule