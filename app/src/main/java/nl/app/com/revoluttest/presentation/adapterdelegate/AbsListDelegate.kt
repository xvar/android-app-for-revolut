package nl.app.com.revoluttest.presentation.adapterdelegate

import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

abstract class AbsListDelegate<I, T, VH, P> : AbsListItemAdapterDelegate<I, T, VH>()
    where VH : DelegateViewHolder<I, P>, I: T {

    override fun onBindViewHolder(item: I, holder: VH, payloads: MutableList<Any>) {
        holder.bind(item, payloads)
    }
}