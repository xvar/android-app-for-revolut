package nl.app.com.revoluttest.presentation.currency.ui

import android.os.Bundle
import nl.app.com.revoluttest.R
import nl.app.com.revoluttest.presentation.activity.BaseActivity

class CurrencyActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.currency_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, CurrencyFragment.newInstance())
                    .commit()
        }
    }

}
