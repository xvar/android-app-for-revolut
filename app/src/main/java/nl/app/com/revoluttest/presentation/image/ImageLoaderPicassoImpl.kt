package nl.app.com.revoluttest.presentation.image

import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation

class ImageLoaderPicassoImpl(private val picasso: Picasso) : ImageLoader {

    override fun loadImage(url: String, sidePx: Int, target: ImageView, transformation: Transformation?) {
        picasso.load(url)
                .resize(sidePx, sidePx)
                .centerCrop()
                .apply {
                    transformation?.let { transform(it) }
                }
                .into(target)
    }

    override fun loadImage(id: Int, sidePx: Int, target: ImageView, transformation: Transformation?) {
        picasso.load(id)
                .resize(sidePx, sidePx)
                .centerCrop()
                .apply {
                    transformation?.let { transform(it) }
                }
                .into(target)
    }

}