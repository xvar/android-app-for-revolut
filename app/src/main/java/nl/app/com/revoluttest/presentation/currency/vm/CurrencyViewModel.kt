package nl.app.com.revoluttest.presentation.currency.vm

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers
import nl.app.com.revoluttest.domain.InteractorFactory
import nl.app.com.revoluttest.presentation.currency.model.Currency
import nl.app.com.revoluttest.presentation.currency.model.UIEvent
import nl.app.com.revoluttest.presentation.currency.model.UIState
import nl.app.com.revoluttest.presentation.currency.model.textId
import javax.inject.Inject

private const val KEY_CURR = "cur_sub"
class CurrencyViewModel @Inject constructor(inF: InteractorFactory) : BaseViewModel(), Consumer<UIEvent> {

    private val interactor = inF.provideInteractor()
    private val ui = BehaviorProcessor.create<UIState>()

    override fun accept(event: UIEvent) {
        when (event) {
            is UIEvent.CurrencyClick -> {
                interactor.changeBase(event.item.code)
            }
            is UIEvent.Edit -> interactor.changeBaseAmount(event.text)
            is UIEvent.EditFinished -> interactor.update()
            else -> { /*ignore*/ }
        }
    }

    fun init() {
        addDisposable(KEY_CURR, interactor.create()
                .doOnSubscribe { ui.onNext(UIState.Loading(true)) }
                .subscribe (
                        { state -> ui.onNext(UIState.Loading(false)); ui.onNext(state) },
                        { error -> ui.onNext(UIState.Loading(false)); ui.onNext(UIState.Error(error.textId())) }
                )
        )
    }

    fun getUiState() : Flowable<out UIState> = ui

    override fun onCleared() {
        super.onCleared()
        interactor.clear()
    }
}
