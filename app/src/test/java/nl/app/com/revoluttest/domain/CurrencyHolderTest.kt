package nl.app.com.revoluttest.domain

import nl.app.com.revoluttest.data.CodeMapDTO
import nl.app.com.revoluttest.data.CurrenciesDTO
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class CurrencyHolderTest {

    lateinit var holder : CurrencyHolder

    @Before
    fun setUp() {
        holder = CurrencyHolder()
    }

    @After
    fun tearDown() {
    }

    @Test
    fun apply_init() {
        val dto = init()
        val result = holder.apply(dto)

        assertEquals(4, result.size)
        val expected = listOf<CurrencyDomain>(
                CurrencyDomain("EUR", 100f, 0),
                CurrencyDomain("RUR", 80 * 100f, 1),
                CurrencyDomain("UAN", 25 * 100f, 2),
                CurrencyDomain("TEST", 50f, 3)
        )
        expected.forEachIndexed { index, el -> assertEquals(el, result[index] ) }
    }

    @Test
    fun change_base() {
        val dto = init()
        holder.apply(dto)

        holder.changeBase("RUR")
        val result = holder.getCurrent()

        assertEquals(4, result.size)
        val expected = listOf<CurrencyDomain>(
                CurrencyDomain("RUR", 80 * 100f, 0),
                CurrencyDomain("EUR", 100f, 1),
                CurrencyDomain("UAN", 25 * 100f, 2),
                CurrencyDomain("TEST", 50f, 3)
        )
        expected.forEachIndexed { index, el -> assertEquals(el, result[index] ) }
    }

    @Test
    fun test_change_multiplier() {
        holder.apply(init())
        val result = holder.apply(changed())

        assertEquals(4, result.size)
        val expected = listOf<CurrencyDomain>(
                CurrencyDomain("EUR", 100f, 0),
                CurrencyDomain("RUR", 81 * 100f, 1),
                CurrencyDomain("UAN", 26 * 100f, 2),
                CurrencyDomain("TEST", 60f, 3)
        )
        expected.forEachIndexed { index, el -> assertEquals(el.amount, result[index].amount, 0.01f) }
    }

    private fun init(): CurrenciesDTO {
        val list = listOf<CodeMapDTO>(
                CodeMapDTO("RUR", 80.0f),
                CodeMapDTO("UAN", 25.0f),
                CodeMapDTO("TEST", 0.5f)
        )
        val dto = CurrenciesDTO("EUR", "2010-12-20", list)
        return dto
    }

    private fun changed(): CurrenciesDTO {
        val list = listOf<CodeMapDTO>(
                CodeMapDTO("RUR", 81.0f),
                CodeMapDTO("UAN", 26.0f),
                CodeMapDTO("TEST", 0.6f)
        )
        val dto = CurrenciesDTO("EUR", "2010-12-20", list)
        return dto
    }


}